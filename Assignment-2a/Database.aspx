﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Database.aspx.cs" Inherits="Assignment_2a.Database" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
   <h2><u>Database</u></h2> 
   <p>It’s a structured system to put your data in that imposes rules upon that data, and the rules are yours, because the importance of these problems changes based on your needs.
      Maybe your problem is the size, while someone else has a smaller amount of data where the sensitivity is a high concern.
      It’s the things you can’t see that are going on in the background; the security, the enforced integrity of the data, the ability to get to it fast and get to it reliably.
   <p>
        </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent2" runat="server">
    <div class="jumbotron">
    <h2><u>Joins</u></h2> 
    <h6> select vendor_name as "ven_name",line_item_description as "li_desc"</h6><br />
    <h6>from vendors ven</h6><br />
    <h6>join  invoices invc</h6><br />
    <h6>on ven.vendor_id=invc.vendor_id</h6><br />
    <h6>join invoice_line_items inli</h6>
    <h6>on invc.invoice_id=inli.invoice_id</h6><br />
    <h6>where invc.invoice_id is not null;</h6><br />
        </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent3" runat="server">
    <div class="jumbotron">
        <h2><u>Create Tables</u></h2> 
        create table books(<br />
         book_id number primary key,<br />
         author_name varchar(45) not null,<br />
         book_name varchar(45)not null<br />
); <br />

insert into books<br /> 
values(1,'Pankaj','database');<br />

insert into books <br />
values(2,'Pankaj','javascript');<br />

insert into books <br />
values(3,'Aroon','project Management');<br />

    
        </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent4" runat="server">
    <div class="jumbotron">
    <h6>Useful Links:</h6>
        <a href="https://medium.com/omarelgabrys-blog/database-introduction-part-1-4844fada1fb0">Database link 1</a><br/><br/><br/><br/>
        <a href="https://www.tutorialspoint.com/dbms/">Database link 2</a><br/><br/><br/><br/>
        <a href="https://www.javatpoint.com/what-is-database">Database link 3</a><br/><br/><br/><br/>
        </div>
</asp:Content>
